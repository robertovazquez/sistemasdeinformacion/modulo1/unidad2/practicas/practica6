USE practica6;


-- 1 consulta

SELECT 
  * 
FROM tema t 
WHERE t.descripcion='Bases de datos';

SELECT 
  * 
FROM articulo a 
WHERE a.anio='1990';

SELECT 
  c2.titulo_art 
FROM (SELECT * FROM tema t WHERE t.descripcion='Bases de datos') c1
JOIN (SELECT * FROM articulo a WHERE a.anio='1990') c2
 USING (codtema);


-- 3 consulta
  
  -- c1
  SELECT r.referencia FROM revista r;

  
  SELECT * FROM tema t WHERE t.descripcion<>'Medicina';


  -- c2
  SELECT a.referencia FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 JOIN articulo a USING (codtema);

  
  SELECT c1.referencia FROM (SELECT r.referencia FROM revista r) c1 LEFT JOIN (SELECT a.referencia FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 JOIN articulo a USING (codtema)) c2
    USING (referencia) WHERE c2.referencia IS NULL;

  -- final
  SELECT r.titulo_rev FROM (
    SELECT c1.referencia FROM (SELECT r.referencia FROM revista r) c1 LEFT JOIN (SELECT a.referencia FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 JOIN articulo a USING (codtema)) c2
    USING (referencia) WHERE c2.referencia IS NULL)
    c1
    JOIN 
    revista r
    USING (referencia);


-- consulta 4

  -- c1:

  SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1991') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema);
  
  -- c2
  SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1992') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema);

  SELECT a.nombre FROM 
    (
    SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1991') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema)
    ) c1
    JOIN
    (
    SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1992') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema)
    ) c2
    USING (dni)
    JOIN autor a
    USING (dni);


  
-- 5 consulta

-- c1: 
SELECT * FROM articulo a WHERE a.anio='1993';


-- c2:
SELECT * FROM autor a WHERE a.universidad='Politecnica de Madrid';


-- final
SELECT 
  c1.titulo_art 
FROM (SELECT * FROM articulo a WHERE a.anio='1993') c1
JOIN (SELECT * FROM autor a WHERE a.universidad='Politecnica de Madrid') c2
USING (dni);
