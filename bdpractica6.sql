﻿USE practica6;

-- introduciendo datos
INSERT INTO autor (dni, nombre, universidad)
  VALUES ('1', 'pepe', 'Politecnica de Madrid'),
         ('2', 'maria', 'Universidad de Cantabria'),
         ('3', 'paco', 'Universidad de Cantabria'),
         ('4', 'cintia', 'Politecnica de Madrid')
;

INSERT INTO tema (codtema, descripcion)
  VALUES ('01', 'Bases de datos'),
         ('02', 'Medicina'),
         ('03', 'SQL')
       

INSERT INTO revista (referencia, titulo_rev, editorial)
  VALUES ('001', 'SQL How', 'tecnitorial'),
         ('002', 'Health Now', 'lifereal')
;

INSERT INTO articulo (referencia, dni, codtema, titulo_art, anio, volumen, numero, paginas)
  VALUES ('001', '1', '01', 'titulo1', 1990, 'v1', 'n1', '680'),
         ('002', '2', '03', 'titulo2', 1990, 'v2', 'n2', '720'),
         ('001', '3', '02', 'titulo3', 1990, 'v3', 'n3', '60'),
         ('001', '2', '01', 'titulo4', 1991, 'v4', 'n4', '30'),
         ('002', '1', '01', 'titulo5', 1991, 'v5', 'n5', '450'),
         ('002', '4', '01', 'titulo6', 1991, 'v6', 'n6', '100'),
         ('002', '2', '01', 'titulo7', 1992, 'v7', 'n7', '78'),
         ('001', '3', '03', 'titulo8', 1992, 'v8', 'n8', '6'),
         ('002', '4', '03', 'titulo9', 1993, 'v9', 'n9', '680')
;
