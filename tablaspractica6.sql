DROP DATABASE IF EXISTS practica6;
CREATE DATABASE practica6;
USE practica6;

CREATE OR REPLACE TABLE autor (
  dni varchar(9),
  nombre varchar(100),
  universidad varchar(100),
  PRIMARY KEY (dni)
  );


CREATE OR REPLACE TABLE tema (
  codtema varchar(9),
  descripcion varchar(100),
  PRIMARY KEY (codtema)
);

CREATE OR REPLACE TABLE revista (
  referencia varchar(9),
  titulo_rev varchar(100),
  editorial varchar(100),
  PRIMARY KEY (referencia)
  );


CREATE OR REPLACE TABLE articulo (
  referencia varchar(9),
  dni varchar(9),
  codtema varchar(9),
  titulo_art varchar(100),
  anio varchar(4), 
  volumen varchar(2),
  numero varchar(4),
  paginas varchar(3),
  PRIMARY KEY (referencia, dni,codtema)
  );

ALTER TABLE articulo
  ADD CONSTRAINT fkarticulo_revista FOREIGN KEY (referencia)
  REFERENCES revista (referencia);


ALTER TABLE articulo
  ADD CONSTRAINT fkarticulo_autor FOREIGN KEY (dni)
  REFERENCES autor (dni);

ALTER TABLE articulo
  ADD CONSTRAINT fkarticulo_tema FOREIGN KEY (codtema)
  REFERENCES tema (codtema);

